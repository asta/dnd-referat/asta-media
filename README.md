# AStA-Media

A simple static side generator to create a interface for AStA videos shared via the AStA Nextcloud.
This repository is only for styling and generating the website, the conetent must be managed on 
[https://cloud.asta.uni-goettingen.de/apps/files/](cloud.asta.uni-goettingen.de) using the media account. 

## Usage:

### add new Video
*please try not to work to much in the nextcloud online editor, this always starts a new gitlab-pipeline*
1. upload video(mp4) and cover image(1:1) to nextcloud in the folder media
2. share both files using nextcloud to create public links
3. create a text fraction similar to this:
```YAML
-
 name: <name of the video>
 beschreibung: <description of the video>
 bildurl: <public link for the cover image(step 2)>
 videourl: <public link for the video(step 2)>
 datum: <date of the presentation>

```
4. add this text fraction ad the end of the mediaArchiv.txt file <br>
**The whitespaces and line breaks are important !**

If every thing went right the website should be updated in a few minutes

### edit video
all information and links can be changed in the mediaArchiv.txt file.


## Workflow:
By changing the mediaArichv.txt file a webhook triggers the pipeline of this gitlab project, here the pipeline tries to create the page. If this works out an other webhook triggers caprover to create a new version of the docker image based on this repository. 
