import urllib.request
import yaml
import shutil
from datetime import datetime
from jinja2 import Template

# load yaml file "videoArchiv.txt" from nextcloud media account
url ="https://cloud.asta.uni-goettingen.de/s/3ocgjGAEejdcxx5/download"
x = urllib.request.urlopen(url)
#parse to list
liste = yaml.load(x.read(), Loader=yaml.SafeLoader)

#sort list by date
liste= sorted(liste, key=lambda x: datetime. strptime(x['datum'],'%d.%m.%y'),reverse=True)


#load jinja2 Templates
with open('template/index.html') as file_:
        template = Template(file_.read())
 

with open('template/video.html') as file_:
        videoTemplate = Template(file_.read())

#load static components (css/ asta-logo)
shutil.copytree("template/static","output/static",dirs_exist_ok=True)


#create Index output 
f=open("output/index.html","w")
f.write(template.render(mediaBlocks=liste))
f.close()

# create static video files
for media in liste:
        print()
        f=open("output/"+media['name'].replace('"','')+".html","w")
        f.write(videoTemplate.render(title=media['name'], link=media['videourl']))
        f.close()

        
        